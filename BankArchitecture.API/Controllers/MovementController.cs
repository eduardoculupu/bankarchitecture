﻿using BankArchitecture.Application.Features.Accounts.Commands.CreateAccount;
using BankArchitecture.Application.Features.Accounts.Commands.DeleteAccount;
using BankArchitecture.Application.Features.Accounts.Commands.UpdateAccount;
using BankArchitecture.Application.Features.Movements.Commands.CreateMovement;
using BankArchitecture.Application.Features.Movements.Commands.DeleteMovement;
using BankArchitecture.Application.Features.Movements.Commands.UpdateMovement;
using BankArchitecture.Application.Features.Movements.Queries.GetMovementListDateCustomer;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace BankArchitecture.API.Controllers
{
    [ApiController]
    [Route("api/v1/movimientos")]
    public class MovementController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MovementController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost(Name = "CreateMovement")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> CreateMovement([FromBody] CreateMovementCommand command)
        {
            return await _mediator.Send(command);
        }


        [HttpPatch(Name = "UpdateMovement")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> UpdateMovement([FromBody] UpdateMovementCommand command)
        {
            await _mediator.Send(command);
            return NoContent();
        }

        [HttpDelete(Name = "DeleteMovement")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> DeleteMovement(int MovimientoID)
        {
            var command = new DeleteMovementCommand
            {
                MovimientoID = MovimientoID
            };
            await _mediator.Send(command);
            return NoContent();
        }

        [HttpGet(Name = "reportes")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<IEnumerable<MovementsDateCustomer>>> ReportMovement(DateTime FechaInicio, DateTime FechaFin, int ClienteID)
        {
            var query = new GetMovementListDateCustomerQuery
            {
                FechaInicio = FechaInicio,
                FechaFin = FechaFin,
                ClienteID = ClienteID,
            };
            var resp = await _mediator.Send(query);
            return Ok(resp);
        }
    }
}
