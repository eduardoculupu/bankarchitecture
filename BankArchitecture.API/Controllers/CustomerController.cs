﻿using BankArchitecture.Application.Features.Customers.Commands.CreateCustomer;
using BankArchitecture.Application.Features.Customers.Commands.DeleteCustomer;
using BankArchitecture.Application.Features.Customers.Commands.UpdateCustomer;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace BankArchitecture.API.Controllers
{
    [ApiController]
    [Route("api/v1/clientes")]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CustomerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost(Name ="CreateCustomer")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> CreateCustomer([FromBody] CreateCustomerCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpPatch(Name ="UpdateCustomer")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> UpdateCustomer([FromBody] UpdateCustomerCommand command)
        {
            await _mediator.Send(command);
            return NoContent();
        }

        [HttpDelete(Name ="DeleteCustomer")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> DeleteCustomer(int ClienteID)
        {
            var command = new DeleteCustomerCommand
            {
                ClienteID = ClienteID
            };
            await _mediator.Send(command);
            return NoContent();
        }

    }
}
