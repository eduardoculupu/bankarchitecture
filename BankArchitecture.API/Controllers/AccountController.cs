﻿using BankArchitecture.Application.Features.Accounts.Commands.CreateAccount;
using BankArchitecture.Application.Features.Accounts.Commands.DeleteAccount;
using BankArchitecture.Application.Features.Accounts.Commands.UpdateAccount;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace BankArchitecture.API.Controllers
{
    [ApiController]
    [Route("api/v1/cuentas")]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost(Name = "CreateAccount")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<int>> CreateAccount([FromBody] CreateAccountCommand command)
        {
            return await _mediator.Send(command);
        }


        [HttpPatch(Name = "UpdateAccount")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> UpdateAccount([FromBody] UpdateAccountCommand command)
        {
            await _mediator.Send(command);
            return NoContent();
        }

        [HttpDelete(Name = "DeleteAccount")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> DeleteAccount(int CuentaID)
        {
            var command = new DeleteAccountCommand
            {
                CuentaID = CuentaID
            };
            await _mediator.Send(command);
            return NoContent();
        }
    }
}
