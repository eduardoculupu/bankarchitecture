﻿using BankArchitecture.Domain.Common;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BankArchitecture.Application.Contracts.Persistence
{
    public interface IAsyncRepository<T> where T : BaseDomainModel
    {
        #region Obtener un record por id
        Task<T> GetByIdAsync(int id);
        #endregion

        Task<IReadOnlyList<T>> GetAsync(Expression<Func<T, bool>> predicate);

        #region Agregar un nuevo record
        Task<T> AddAsync(T entity);
        #endregion

        #region Actualizar un nuevo record
        Task<T> UpdateAsync(T entity);
        #endregion

        #region Eliminar un record
        Task DeleteAsync(T entity);
        #endregion

        void AddEntity(T entity);
        void UpdateEntity(T entity);
        void DeleteEntity(T entity);
    }
}
