﻿using BankArchitecture.Domain;

namespace BankArchitecture.Application.Contracts.Persistence
{
    public interface IPersonRepository : IAsyncRepository<Person>
    {
    }
}
