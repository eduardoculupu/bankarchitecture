﻿using BankArchitecture.Domain;

namespace BankArchitecture.Application.Contracts.Persistence
{
    public interface ICustomerRepository : IAsyncRepository<Customer>
    {
    }
}
