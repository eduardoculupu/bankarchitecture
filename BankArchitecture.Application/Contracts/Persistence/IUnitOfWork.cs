﻿using BankArchitecture.Domain.Common;

namespace BankArchitecture.Application.Contracts.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        ICustomerRepository CustomerRepository { get; }
        IMovementRepository MovementRepository { get; }
        IPersonRepository PersonRepository { get; }
        IAsyncRepository<TEntity> Repository<TEntity>() where TEntity : BaseDomainModel;

        Task<int> Complete();
    }
}
