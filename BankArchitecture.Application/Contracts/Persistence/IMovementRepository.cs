﻿using BankArchitecture.Application.Features.Movements.Queries.GetMovementListDateCustomer;
using BankArchitecture.Domain;

namespace BankArchitecture.Application.Contracts.Persistence
{
    public interface IMovementRepository : IAsyncRepository<Movement>
    {
        Task<IEnumerable<Movement>> GetMovementByAccount(int CuentaID);

        Task<IEnumerable<MovementsDateCustomer>> GetMovementByDateAccount(DateTime FechaInicio, DateTime FechaFin, int ClienteID);
    }
}
