﻿using AutoMapper;
using BankArchitecture.Application.Features.Accounts.Commands.CreateAccount;
using BankArchitecture.Application.Features.Accounts.Commands.UpdateAccount;
using BankArchitecture.Application.Features.Customers.Commands.CreateCustomer;
using BankArchitecture.Application.Features.Customers.Commands.UpdateCustomer;
using BankArchitecture.Application.Features.Movements.Commands.CreateMovement;
using BankArchitecture.Application.Features.Movements.Commands.UpdateMovement;
using BankArchitecture.Application.Features.Movements.Queries.GetMovementList;
using BankArchitecture.Domain;

namespace BankArchitecture.Application.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile() 
        {
            CreateMap<CreateCustomerCommand, Customer>();
            CreateMap<CreateCustomerCommand, Person>();

            CreateMap<UpdateCustomerCommand, Customer>();
            CreateMap<UpdateCustomerCommand, Person>();
            
            CreateMap<CreateAccountCommand, Account>();
            CreateMap<UpdateAccountCommand, Account>();

            CreateMap<CreateMovementCommand, Movement>();
            CreateMap<UpdateMovementCommand, Movement>();

            CreateMap<Movement, MovementsVm>();
            
        }
    }
}
