﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Customers.Commands.CreateCustomer
{
    public class CreateCustomerCommandValidator : AbstractValidator<CreateCustomerCommand>
    {
        public CreateCustomerCommandValidator()
        {
            RuleFor(p => p.Nombre)
                .NotEmpty().WithMessage("{Nombre} no puede estar en blanco")
                .NotNull().WithMessage("{Nombre} no puede estar en blanco");

            RuleFor(p => p.Identificacion)
                .NotEmpty().WithMessage("{Identificación no puede estar en blanco}")
                .NotNull().WithMessage("{Nombre} no puede estar en blanco");

            RuleFor(p => p.Genero)
                .MaximumLength(1).WithMessage("{Genero} excede los caracteres permitidos")
                .NotEmpty().WithMessage("{Nombre} no puede estar en blanco")
                .NotNull().WithMessage("{Nombre} no puede estar en blanco");
        }
    }
}
