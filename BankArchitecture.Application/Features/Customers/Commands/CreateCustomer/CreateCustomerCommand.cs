﻿using BankArchitecture.Domain;
using MediatR;

namespace BankArchitecture.Application.Features.Customers.Commands.CreateCustomer
{
    public class CreateCustomerCommand : IRequest<int>
    {
        public string? Contrasena { get; set; }
        public bool Estado { get; set; }

        public string? Nombre { get; set; }
        public string Genero { get; set; } = string.Empty;
        public int Edad { get; set; }
        public string Identificacion { get; set; } = string.Empty;
        public string Direccion { get; set; } = string.Empty;
        public string Telefono { get; set; } = string.Empty;

    }
}
