﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Customers.Commands.UpdateCustomer
{
    public class UpdateCustomerCommandValidator : AbstractValidator<UpdateCustomerCommand>
    {
        public UpdateCustomerCommandValidator()
        {
            RuleFor(p => p.Nombre)
                .NotEmpty().WithMessage("{Nombre} no puede estar en blanco")
                .NotNull().WithMessage("{Nombre} no puede estar en blanco");

            RuleFor(p => p.Identificacion)
                .NotEmpty().WithMessage("{Identificación no puede estar en blanco}")
                .NotNull().WithMessage("{Nombre} no puede estar en blanco");

            RuleFor(p => p.Genero)
                .Length(1).WithMessage("{Genero} excede los caracteres permitidos");
        }
    }
}
