﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Application.Exceptions;
using BankArchitecture.Domain;
using MediatR;

namespace BankArchitecture.Application.Features.Customers.Commands.UpdateCustomer
{
    public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateCustomerCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customerToUpdate = await _unitOfWork.Repository<Customer>().GetByIdAsync(request.ClienteID);
            if (customerToUpdate == null)
            {
                throw new NotFoundException(nameof(Customer), request.ClienteID);
            }

            var personToUpdate = await _unitOfWork.Repository<Person>().GetByIdAsync(customerToUpdate.PersonID);

            _mapper.Map(request, customerToUpdate, typeof(UpdateCustomerCommand), typeof(Customer));
            _mapper.Map(request, personToUpdate, typeof(UpdateCustomerCommand), typeof(Person));
            customerToUpdate.Person = personToUpdate;

            _unitOfWork.Repository<Customer>().UpdateEntity(customerToUpdate);
            await _unitOfWork.Complete();

            return Unit.Value;
        }
    }

}
