﻿using MediatR;

namespace BankArchitecture.Application.Features.Customers.Commands.UpdateCustomer
{
    public class UpdateCustomerCommand : IRequest
    {
        public int ClienteID { get; set; }

        public string Nombre { get; set; } = string.Empty;
        public string? Genero { get; set; } = string.Empty;
        public int Edad { get; set; }
        public string? Identificacion { get; set; } = string.Empty;
        public string? Direcccion { get; set; } = string.Empty;
        public string? Telefono { get; set; } = string.Empty;

        public string? Contrasena { get; set; } = string.Empty;
        public bool Estado { get; set; }
    }
}
