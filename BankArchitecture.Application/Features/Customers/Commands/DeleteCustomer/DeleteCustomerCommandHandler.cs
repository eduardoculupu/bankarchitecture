﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Application.Exceptions;
using BankArchitecture.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Customers.Commands.DeleteCustomer
{
    public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand>
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;

        public DeleteCustomerCommandHandler(ICustomerRepository customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteCustomerCommand request, CancellationToken cancellationToken)
        {
            var customerToDelete = await _customerRepository.GetByIdAsync(request.ClienteID);
            if (customerToDelete == null)
            {
                throw new NotFoundException(nameof(Customer), request.ClienteID);
            }

            await _customerRepository.DeleteAsync(customerToDelete);

            return Unit.Value;
        }
    }
}
