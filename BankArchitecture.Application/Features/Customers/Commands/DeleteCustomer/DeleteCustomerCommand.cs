﻿using MediatR;

namespace BankArchitecture.Application.Features.Customers.Commands.DeleteCustomer
{
    public class DeleteCustomerCommand : IRequest
    {
        public int ClienteID { get; set; }
    }
}
