﻿using MediatR;

namespace BankArchitecture.Application.Features.Movements.Commands.CreateMovement
{
    public class CreateMovementCommand : IRequest<int>
    {
        public int CuentaID { get; set; }
        public DateTime? Fecha { get; set; }
        public string? TipoMovimiento { get; set; }
        public decimal? Valor { get; set; }
    }
}
