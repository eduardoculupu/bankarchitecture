﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Domain;
using MediatR;

namespace BankArchitecture.Application.Features.Movements.Commands.CreateMovement
{
    public class CreateMovementCommandHandler : IRequestHandler<CreateMovementCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateMovementCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<int> Handle(CreateMovementCommand request, CancellationToken cancellationToken)
        {
            //verificamos si tiene una cuenta
            var initialBalanceEntity = await _unitOfWork.Repository<Account>().GetByIdAsync(request.CuentaID);
            if(initialBalanceEntity == null)
            {
                throw new Exception("No existe la cuenta");
            }
            var amountSaldo = initialBalanceEntity.SaldoInicial;
            //obtenemos el ultimo saldo, si no tiene movimientos tomaremos el saldo inicial de la cuenta
            var listMovements = await _unitOfWork.MovementRepository.GetMovementByAccount(request.CuentaID);
            if(listMovements.Count() > 0)
            {
                amountSaldo = (decimal)listMovements.First().Saldo;
            }

            var movementEntity = _mapper.Map<Movement>(request);

            if (movementEntity.Valor > amountSaldo && movementEntity.TipoMovimiento == "D")
            {
                throw new Exception("Saldo no Disponible");
            }

            if (request.TipoMovimiento == "C")
            {
                movementEntity.Saldo = amountSaldo + movementEntity.Valor;
            }
            if (request.TipoMovimiento == "D")
            {
                movementEntity.Saldo = amountSaldo - movementEntity.Valor;
                movementEntity.Valor = -movementEntity.Valor;
            }


            _unitOfWork.Repository<Movement>().AddEntity(movementEntity);

            


            var result = await _unitOfWork.Complete();
            if (result <= 0)
            {
                throw new Exception("No se pudo insertar el movimiento de la cuenta");
            }

            return movementEntity.MovimientoID;
        }


    }
}
