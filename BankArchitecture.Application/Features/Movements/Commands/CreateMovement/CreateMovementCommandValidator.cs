﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Movements.Commands.CreateMovement
{
    public class CreateMovementCommandValidator : AbstractValidator<CreateMovementCommand>
    {
        public CreateMovementCommandValidator() {
            RuleFor(p => p.CuentaID)
               .NotNull().WithMessage("{NumeroCuenta} no puede ser nulo")
               .NotEmpty().WithMessage("{NumeroCuenta} no puede ser vacio");

            RuleFor(p => p.TipoMovimiento)
               .NotNull().WithMessage("{NumeroCuenta} no puede ser nulo")
               .NotEmpty().WithMessage("{NumeroCuenta} no puede ser vacio");

        }
    }
}
