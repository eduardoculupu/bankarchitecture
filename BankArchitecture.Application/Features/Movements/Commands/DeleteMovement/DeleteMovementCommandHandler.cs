﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Application.Exceptions;
using BankArchitecture.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Movements.Commands.DeleteMovement
{
    public class DeleteMovementCommandHandler : IRequestHandler<DeleteMovementCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DeleteMovementCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteMovementCommand request, CancellationToken cancellationToken)
        {
            var movementToDelete = await _unitOfWork.Repository<Movement>().GetByIdAsync(request.MovimientoID);
            if (movementToDelete == null)
            {
                throw new NotFoundException(nameof(Movement), request.MovimientoID);
            }

            _unitOfWork.Repository<Movement>().DeleteEntity(movementToDelete);

            await _unitOfWork.Complete();

            return Unit.Value;
        }
    }
}
