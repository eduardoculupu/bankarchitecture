﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Movements.Commands.DeleteMovement
{
    public class DeleteMovementCommand : IRequest
    {
        public int MovimientoID { get; set; }
    }
}
