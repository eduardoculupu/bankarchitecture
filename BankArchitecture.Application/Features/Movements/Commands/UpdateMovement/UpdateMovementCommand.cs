﻿using MediatR;

namespace BankArchitecture.Application.Features.Movements.Commands.UpdateMovement
{
    public class UpdateMovementCommand : IRequest
    {
        public int MovimientoID { get; set; }
        public DateTime? Fecha { get; set; }
        public string? TipoMovimiento { get; set; }
        public decimal? Valor { get; set; }
    }
}
