﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Application.Exceptions;
using BankArchitecture.Application.Features.Accounts.Commands.UpdateAccount;
using BankArchitecture.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Movements.Commands.UpdateMovement
{
    public class UpdateMovementCommandHandler : IRequestHandler<UpdateMovementCommand>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateMovementCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }


        public async Task<Unit> Handle(UpdateMovementCommand request, CancellationToken cancellationToken)
        {
            var movementToUpdate = await _unitOfWork.Repository<Movement>().GetByIdAsync(request.MovimientoID);

            if (movementToUpdate == null)
            {
                throw new NotFoundException(nameof(Movement), request.MovimientoID);
            }

            _mapper.Map(request, movementToUpdate, typeof(UpdateMovementCommand), typeof(Movement));

            _unitOfWork.Repository<Movement>().UpdateEntity(movementToUpdate);
            await _unitOfWork.Complete();

            return Unit.Value;
        }
    }
}
