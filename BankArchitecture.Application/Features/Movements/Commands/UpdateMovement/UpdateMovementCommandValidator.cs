﻿using FluentValidation;
using MediatR;

namespace BankArchitecture.Application.Features.Movements.Commands.UpdateMovement
{
    public class UpdateMovementCommandValidator : AbstractValidator<UpdateMovementCommand>
    {
        public UpdateMovementCommandValidator()
        {

            RuleFor(p => p.TipoMovimiento)
               .NotNull().WithMessage("{NumeroCuenta} no puede ser nulo")
               .NotEmpty().WithMessage("{NumeroCuenta} no puede ser vacio");

        }
    }
}
