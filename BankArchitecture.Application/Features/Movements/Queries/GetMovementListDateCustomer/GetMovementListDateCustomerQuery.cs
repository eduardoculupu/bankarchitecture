﻿using MediatR;

namespace BankArchitecture.Application.Features.Movements.Queries.GetMovementListDateCustomer
{
    public class GetMovementListDateCustomerQuery : IRequest<List<MovementsDateCustomer>>
    {
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public int ClienteID { get; set; }
    }
}
