﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Domain;
using MediatR;

namespace BankArchitecture.Application.Features.Movements.Queries.GetMovementListDateCustomer
{
    public class GetMovementListDateCustomerQueryHandler : IRequestHandler<GetMovementListDateCustomerQuery, List<MovementsDateCustomer>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetMovementListDateCustomerQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<MovementsDateCustomer>> Handle(GetMovementListDateCustomerQuery request, CancellationToken cancellationToken)
        {
            var resp = await _unitOfWork.MovementRepository.GetMovementByDateAccount(request.FechaInicio, request.FechaFin, request.ClienteID);
            return (List<MovementsDateCustomer>)resp;
        }
    }
}
