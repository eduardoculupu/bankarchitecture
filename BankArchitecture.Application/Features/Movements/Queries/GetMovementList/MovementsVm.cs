﻿namespace BankArchitecture.Application.Features.Movements.Queries.GetMovementList
{
    public class MovementsVm
    {
        public int MovimientoID { get; set; }
        public int CuentaID { get; set; }
        public DateTime? Fecha { get; set; }
        public string? TipoMovimiento { get; set; }
        public decimal? Valor { get; set; }
    }
}
