﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using MediatR;

namespace BankArchitecture.Application.Features.Movements.Queries.GetMovementList
{
    public class GetMovementListQueryHandler : IRequestHandler<GetMovementListQuery, List<MovementsVm>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetMovementListQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<MovementsVm>> Handle(GetMovementListQuery request, CancellationToken cancellationToken)
        {
            var movementList = await _unitOfWork.MovementRepository.GetMovementByAccount(request._CuentaID);

            return _mapper.Map<List<MovementsVm>>(movementList);
        }
    }
}
