﻿using MediatR;

namespace BankArchitecture.Application.Features.Movements.Queries.GetMovementList
{
    public class GetMovementListQuery : IRequest<List<MovementsVm>>
    {
        public int _CuentaID { get; set; }

        public GetMovementListQuery(int cuentaID)
        {
            _CuentaID = cuentaID;
        }
    }
}
