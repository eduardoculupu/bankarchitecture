﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Domain;
using MediatR;

namespace BankArchitecture.Application.Features.Accounts.Commands.CreateAccount
{
    public class CreateAccountCommandHandler : IRequestHandler<CreateAccountCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateAccountCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(CreateAccountCommand request, CancellationToken cancellationToken)
        {
            var accountEntity = _mapper.Map<Account>(request);
            _unitOfWork.Repository<Account>().AddEntity(accountEntity);
            var result = await _unitOfWork.Complete();
            if(result <= 0)
            {
                throw new Exception("No se pudo insertar el record de la cuenta");
            }

            return accountEntity.CuentaID;
        }
    }
}
