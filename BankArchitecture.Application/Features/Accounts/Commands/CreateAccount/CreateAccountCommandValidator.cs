﻿using FluentValidation;

namespace BankArchitecture.Application.Features.Accounts.Commands.CreateAccount
{
    public class CreateAccountCommandValidator : AbstractValidator<CreateAccountCommand>
    {
        public CreateAccountCommandValidator()
        {
            RuleFor(p => p.NumeroCuenta)
                .NotNull().WithMessage("{NumeroCuenta} no puede ser nulo")
                .NotEmpty().WithMessage("{NumeroCuenta} no puede ser vacio");

            RuleFor(p => p.TipoCuenta)
               .NotNull().WithMessage("{TipoCuenta} no puede ser nulo")
               .NotEmpty().WithMessage("{TipoCuenta} no puede ser vacio");

            //RuleFor(p => p.SaldoInicial)
            //   .NotNull().WithMessage("{SaldoInicial} no puede ser nulo")
            //   .NotEmpty().WithMessage("{SaldoInicial} no puede ser vacio");

            RuleFor(p => p.Estado)
               .NotNull().WithMessage("{Estado} no puede ser nulo")
               .NotEmpty().WithMessage("{Estado} no puede ser vacio");
        }
    }
}
