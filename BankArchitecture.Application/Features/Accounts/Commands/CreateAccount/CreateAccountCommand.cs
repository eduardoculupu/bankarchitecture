﻿using MediatR;

namespace BankArchitecture.Application.Features.Accounts.Commands.CreateAccount
{
    public class CreateAccountCommand : IRequest<int>
    {
        public int ClienteID { get; set; }
        public string? NumeroCuenta { get; set; }
        public string? TipoCuenta { get; set; }
        public decimal SaldoInicial { get; set; }
        public bool? Estado { get; set; }
    }
}
