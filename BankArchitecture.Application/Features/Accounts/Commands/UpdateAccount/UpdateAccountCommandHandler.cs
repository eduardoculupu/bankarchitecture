﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Application.Exceptions;
using BankArchitecture.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Accounts.Commands.UpdateAccount
{
    public class UpdateAccountCommandHandler : IRequestHandler<UpdateAccountCommand>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateAccountCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateAccountCommand request, CancellationToken cancellationToken)
        {
            var accountToUpdate = await _unitOfWork.Repository<Account>().GetByIdAsync(request.CuentaID);

            if(accountToUpdate == null)
            {
                throw new NotFoundException(nameof(Account), request.CuentaID);
            }

            _mapper.Map(request, accountToUpdate, typeof(UpdateAccountCommand), typeof(Account));

            _unitOfWork.Repository<Account>().UpdateEntity(accountToUpdate);
            await _unitOfWork.Complete();

            return Unit.Value;
        }
    }
}
