﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Accounts.Commands.UpdateAccount
{
    public class UpdateAccountCommandValidator : AbstractValidator<UpdateAccountCommand>
    {
        public UpdateAccountCommandValidator() {
            RuleFor(p => p.NumeroCuenta)
               .NotNull().WithMessage("{NumeroCuenta} no puede ser nulo")
               .NotEmpty().WithMessage("{NumeroCuenta} no puede ser vacio");

            RuleFor(p => p.TipoCuenta)
               .NotNull().WithMessage("{TipoCuenta} no puede ser nulo")
               .NotEmpty().WithMessage("{TipoCuenta} no puede ser vacio");

            RuleFor(p => p.SaldoInicial)
               .NotNull().WithMessage("{SaldoInicial} no puede ser nulo")
               .NotEmpty().WithMessage("{SaldoInicial} no puede ser vacio");

            RuleFor(p => p.Estado)
               .NotNull().WithMessage("{Estado} no puede ser nulo")
               .NotEmpty().WithMessage("{Estado} no puede ser vacio");
        }
    }
}
