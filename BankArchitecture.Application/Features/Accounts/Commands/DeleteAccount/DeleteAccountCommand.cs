﻿using MediatR;

namespace BankArchitecture.Application.Features.Accounts.Commands.DeleteAccount
{
    public class DeleteAccountCommand : IRequest
    {
        public int CuentaID { get; set; }
    }
}
