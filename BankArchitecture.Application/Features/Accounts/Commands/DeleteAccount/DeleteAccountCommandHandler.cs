﻿using AutoMapper;
using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Application.Exceptions;
using BankArchitecture.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Application.Features.Accounts.Commands.DeleteAccount
{
    public class DeleteAccountCommandHandler : IRequestHandler<DeleteAccountCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DeleteAccountCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteAccountCommand request, CancellationToken cancellationToken)
        {
            var accountToDelete = await _unitOfWork.Repository<Account>().GetByIdAsync(request.CuentaID);
            if(accountToDelete == null)
            {
                throw new NotFoundException(nameof(Account), request.CuentaID);
            }

            _unitOfWork.Repository<Account>().DeleteEntity(accountToDelete);

            await _unitOfWork.Complete();

            return Unit.Value;
        }
    }
}
