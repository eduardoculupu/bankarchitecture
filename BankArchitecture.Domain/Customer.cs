﻿using BankArchitecture.Domain.Common;
using System.Text.Json.Serialization;

namespace BankArchitecture.Domain
{
    public class Customer : BaseDomainModel
    {
        public int ClienteID { get; set; }
        public string? Contrasena { get; set; }
        public bool Estado { get; set; }
        public int PersonID { get; set; }

        public ICollection<Account>? Accounts { get; set; }
        public virtual Person? Person { get; set; }
    }
}
