﻿using BankArchitecture.Domain.Common;

namespace BankArchitecture.Domain
{
    public class Movement : BaseDomainModel
    {
        public int MovimientoID { get; set; }
        public int CuentaID { get; set; }
        public DateTime? Fecha { get; set;}
        public string? TipoMovimiento { get; set;}
        public decimal? Valor { get; set;}
        public decimal? Saldo { get; set;}

        public virtual Account? Account { get; set; }
    }
}
