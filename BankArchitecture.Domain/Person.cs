﻿using BankArchitecture.Domain.Common;
using System.ComponentModel.DataAnnotations;

namespace BankArchitecture.Domain
{
    public class Person : BaseDomainModel
    {
        public int PersonID { get; set; }
        public string? Nombre { get; set; }
        public string? Genero { get; set; }
        public int Edad { get; set; }
        public string? Identificacion { get; set; }
        public string? Direccion { get; set; }
        public string? Telefono { get; set; }

        public virtual Customer Customer { get; set; }
        
    }
}