﻿using BankArchitecture.Domain.Common;

namespace BankArchitecture.Domain
{
    public class Account : BaseDomainModel
    {
        public int CuentaID { get; set; }
        public int ClienteID { get; set; }
        public string? NumeroCuenta { get; set; }
        public string? TipoCuenta { get; set; }
        public decimal SaldoInicial { get; set; }
        public bool? Estado { get; set; }

        public virtual Customer? Customer { get; set; }
        public virtual ICollection<Movement>? Movements { get; set; }

    }
}
