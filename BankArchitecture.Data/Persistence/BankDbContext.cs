﻿using BankArchitecture.Domain;
using BankArchitecture.Domain.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.IO;

namespace BankArchitecture.Infrastructure.Persistence
{
    public class BankDbContext : DbContext
    {
        public BankDbContext(DbContextOptions<BankDbContext> options) : base(options)
        {
            
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var entry in ChangeTracker.Entries<BaseDomainModel>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedDate = DateTime.Now;
                        entry.Entity.CreatedBy = "system";
                        break;

                    case EntityState.Modified:
                        entry.Entity.UpdatedDate = DateTime.Now;
                        entry.Entity.UpdatedBy = "system";
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Person>()
            .ToTable("Persons")
            .HasKey(p => p.PersonID);

            modelBuilder.Entity<Customer>()
                .HasOne(m => m.Person)
                .WithOne(m => m.Customer)
                .HasForeignKey<Customer>(p => p.PersonID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Customer>()
                .HasMany(m => m.Accounts)
                .WithOne(m => m.Customer)
                .HasForeignKey(p => p.ClienteID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Customer>()
            .HasKey(p => p.ClienteID);


            modelBuilder.Entity<Account>()
                .HasMany(p => p.Movements)
                .WithOne(t => t.Account)
                .HasForeignKey(p => p.CuentaID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Account>()
                .HasKey(p => p.CuentaID);

            modelBuilder.Entity<Movement>()
               .HasKey(p => p.MovimientoID);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Person>? Persons { get; set; }
        public DbSet<Customer>? Customers { get; set; }
        public DbSet<Account>? Accounts { get; set; }
        public DbSet<Movement>? Movements { get; set; }

    }
}