﻿using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Domain;
using BankArchitecture.Infrastructure.Persistence;

namespace BankArchitecture.Infrastructure.Repositories
{
    public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(BankDbContext context) : base(context)
        { }
    }
}
