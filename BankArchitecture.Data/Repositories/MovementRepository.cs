﻿using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Application.Features.Movements.Queries.GetMovementListDateCustomer;
using BankArchitecture.Domain;
using BankArchitecture.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace BankArchitecture.Infrastructure.Repositories
{
    public class MovementRepository : RepositoryBase<Movement>, IMovementRepository
    {
        public MovementRepository(BankDbContext context) : base(context)
        {
        }
        public async Task<IEnumerable<Movement>> GetMovementByAccount(int CuentaID)
        {
            return await _context.Movements!.Where(v => v.CuentaID == CuentaID).OrderBy(v => v.CreatedDate).ToListAsync();
        }

        public async Task<IEnumerable<MovementsDateCustomer>> GetMovementByDateAccount(DateTime FechaInicio, DateTime FechaFin, int ClienteID)
        {
            var query = from a in _context.Accounts
                        join m in _context.Movements
                        on a.CuentaID equals m.CuentaID
                        join c in _context.Customers
                        on a.ClienteID equals c.ClienteID
                        join p in _context.Persons
                        on c.PersonID equals p.PersonID
                        where c.ClienteID == ClienteID
                        where m.Fecha >= FechaInicio
                        where m.Fecha <= FechaFin
                        select new MovementsDateCustomer
                        {
                            Fecha = m.Fecha,
                            Cliente = p.Nombre,
                            NumeroCuenta = a.NumeroCuenta,
                            TipoCuenta = a.TipoCuenta,
                            SaldoInicial = a.SaldoInicial,
                            Estado = (bool)a.Estado,
                            Movimiento = (decimal)m.Valor,
                            SaldoDisponible = (decimal)m.Saldo,
                        };
            //var dat = await _context.Movements!.Include(c => c.Account).ToListAsync();

            //var query = await _context.Customers.Where(c => c.ClienteID == ClienteID)
            //        .Include(c => c.Person)
            //        .Include(c => c.Accounts)
            //        .ThenInclude(c => c.Movements)
            //        .ToListAsync();

            //foreach (var item in query.Select(b => b.Accounts.Select(m => m.Movements)))
            //{
            //    Console.WriteLine("Blog: " + item.);
            //}

            //var data = from a in _context.Movements select 

            return query.ToList();
        }
    }
}
