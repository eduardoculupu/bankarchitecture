﻿using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Domain;
using BankArchitecture.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankArchitecture.Infrastructure.Repositories
{
    public class PersonRepository : RepositoryBase<Person>, IPersonRepository
    {
        public PersonRepository(BankDbContext context) : base(context)
        {
        }
    }
}
