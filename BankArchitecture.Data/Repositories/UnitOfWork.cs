﻿using BankArchitecture.Application.Contracts.Persistence;
using BankArchitecture.Domain.Common;
using BankArchitecture.Infrastructure.Persistence;
using System.Collections;

namespace BankArchitecture.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private Hashtable _repositories;
        private readonly BankDbContext _context;

        private ICustomerRepository _bankRepository;
        private IMovementRepository _movementRepository;
        private IPersonRepository _personRepository;

        public ICustomerRepository CustomerRepository => _bankRepository ?? new CustomerRepository(_context);
        public IMovementRepository MovementRepository => _movementRepository ?? new MovementRepository(_context);
        public IPersonRepository PersonRepository => _personRepository ?? new PersonRepository(_context);

        public UnitOfWork(BankDbContext context)
        {
            _context = context;
        }

        public BankDbContext BankDbContext => _context;


        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public IAsyncRepository<TEntity> Repository<TEntity>() where TEntity : BaseDomainModel
        {
            if(_repositories == null)
            {
                _repositories = new Hashtable();
            }

            var type = typeof(TEntity).Name;

            if(!_repositories.ContainsKey(type))
            {
                var repositoryType = typeof(RepositoryBase<>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), _context);
                _repositories.Add(type, repositoryInstance);
            }

            return (IAsyncRepository<TEntity>)_repositories[type];
        }
    }
}
