﻿using AutoMapper;
using BankArchitecture.Application.Features.Accounts.Commands.CreateAccount;
using BankArchitecture.Application.Mappings;
using BankArchitecture.Application.UnitTests.Mocks;
using BankArchitecture.Infrastructure.Repositories;
using Moq;
using Shouldly;
using Xunit;

namespace BankArchitecture.Application.UnitTests.Features.Accounts.Commands.CreateAccount
{
    public class CreateAccountCommandHandlerXUnitTests
    {
        private readonly IMapper _mapper;
        private readonly Mock<UnitOfWork> _unitOfWork;

        public CreateAccountCommandHandlerXUnitTests()
        {
            _unitOfWork = MockUnitOfWork.GetUnitOfWork();
            var mapperConfig = new MapperConfiguration(c =>
            {
                c.AddProfile<MappingProfile>();
            });
            _mapper = mapperConfig.CreateMapper();

            MockAccountRepository.AddDataAccountRepository(_unitOfWork.Object.BankDbContext);
        }

        [Fact]
        public async Task CreateAccountCommand_InputAccount_ReturnsNumber()
        {
            var accountInput = new CreateAccountCommand
            {
                ClienteID = 15,
                NumeroCuenta = "478758",
                TipoCuenta = "Corriente",
                SaldoInicial = 2000,
                Estado = true
            };

            var handler = new CreateAccountCommandHandler(_mapper, _unitOfWork.Object);

            var result = await handler.Handle(accountInput, CancellationToken.None);

            result.ShouldBeOfType<int>();
        }
    }
}
