﻿using AutoMapper;
using BankArchitecture.Application.Features.Accounts.Commands.UpdateAccount;
using BankArchitecture.Application.Mappings;
using BankArchitecture.Application.UnitTests.Mocks;
using BankArchitecture.Infrastructure.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BankArchitecture.Application.UnitTests.Features.Accounts.Commands.UpdateAccount
{
    public class UpdateAccountCommandHandlerXUnitTests
    {
        private readonly IMapper _mapper;
        private readonly Mock<UnitOfWork> _unitOfWork;

        public UpdateAccountCommandHandlerXUnitTests()
        {
            _unitOfWork = MockUnitOfWork.GetUnitOfWork();
            var mapperConfig = new MapperConfiguration(c =>
            {
                c.AddProfile<MappingProfile>();
            });
            _mapper = mapperConfig.CreateMapper();


            MockAccountRepository.AddDataAccountRepository(_unitOfWork.Object.BankDbContext);
        }

        [Fact]
        public async Task UpdateAccountCommand_InputAccount_ReturnsUnit()
        {
            var streamerInput = new UpdateAccountCommand
            {
                CuentaID = 8000,
                ClienteID = 12,
                NumeroCuenta = "99999",
                TipoCuenta = "Corriente",
                SaldoInicial = 1300,
                Estado = true,
            };

            var handler = new UpdateAccountCommandHandler(_mapper, _unitOfWork.Object);

            var result = await handler.Handle(streamerInput, CancellationToken.None);

            result.ShouldBeOfType<Unit>();
        }
    }
}
