﻿using AutoMapper;
using BankArchitecture.Application.Features.Accounts.Commands.DeleteAccount;
using BankArchitecture.Application.Mappings;
using BankArchitecture.Application.UnitTests.Mocks;
using BankArchitecture.Infrastructure.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BankArchitecture.Application.UnitTests.Features.Accounts.Commands.DeleteAccount
{
    public class DeleteAccountCommandHandlerXUnitTests
    {
        private readonly IMapper _mapper;
        private readonly Mock<UnitOfWork> _unitOfWork;

        public DeleteAccountCommandHandlerXUnitTests()
        {
            _unitOfWork = MockUnitOfWork.GetUnitOfWork();
            var mapperConfig = new MapperConfiguration(c =>
            {
                c.AddProfile<MappingProfile>();
            });
            _mapper = mapperConfig.CreateMapper();

            MockAccountRepository.AddDataAccountRepository(_unitOfWork.Object.BankDbContext);
        }

        [Fact]
        public async Task UpdateStreamerCommand_InputStreamer_ReturnsUnit()
        {
            var streamerInput = new DeleteAccountCommand
            {
                CuentaID = 8000,
            };

            var handler = new DeleteAccountCommandHandler(_unitOfWork.Object, _mapper);

            var result = await handler.Handle(streamerInput, CancellationToken.None);

            result.ShouldBeOfType<Unit>();
        }
    }
}
