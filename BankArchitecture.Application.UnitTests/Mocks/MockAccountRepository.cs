﻿using AutoFixture;
using BankArchitecture.Domain;
using BankArchitecture.Infrastructure.Persistence;

namespace BankArchitecture.Application.UnitTests.Mocks
{
    public static class MockAccountRepository
    {
        public static void AddDataAccountRepository(BankDbContext bankDbContextFake)
        {
            var fixture = new Fixture();
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var accounts = fixture.CreateMany<Account>().ToList();

            accounts.Add(fixture.Build<Account>()
               .With(tr => tr.CuentaID, 8000)
               .Without(tr => tr.Movements)
               .Create()
            );

            bankDbContextFake.Accounts!.AddRange(accounts);
            bankDbContextFake.SaveChanges();

        }
    }
}
