﻿using BankArchitecture.Infrastructure.Persistence;
using BankArchitecture.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace BankArchitecture.Application.UnitTests.Mocks
{
    public static class MockUnitOfWork
    {
        public static Mock<UnitOfWork> GetUnitOfWork()
        {
            Guid dbContextId = Guid.NewGuid();
            var options = new DbContextOptionsBuilder<BankDbContext>()
               .UseInMemoryDatabase(databaseName: $"BankDbContext-{dbContextId}")
               .Options;

            var bankDbContextFake = new BankDbContext(options);

            bankDbContextFake.Database.EnsureDeleted();
            var mockUnitOfWork = new Mock<UnitOfWork>(bankDbContextFake);

            return mockUnitOfWork;
        }
    }
}
